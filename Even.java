//This is a class all about even integers
//It allows you to add, subtract, multiply, divide
//and the results should all be even
//It even allows you to translate ints to a string
//By David Bratkov
public class Even {

	private int value;

	public Even(int x) {

	if(x%2==0) value = x;//Checks to see if even then puts it into the value integer
	
	else value = (x + 1);//Checks to see if odd then puts x + 1 into the value integer

	}

//Allows the other functions to use the int that is private to do math
public int toInt () {
	
	return(value);
	
}


//This is a method that accepts an input and adds it to the value integer and returns it as a Even
public Even add (Even arg) {
		
	Even result = new Even(value + arg.toInt());
	return(result);

}

//This is a method similar to the add but it subtracts
public Even sub (Even arg) {
	
	Even result = new Even (value - arg.toInt());
	return(result);

}

//This is a metho similar to the add but it multiplies
public Even mul (Even arg) {
         
	 Even result = new Even (value * arg.toInt());
         return(result);

}

//This is a method similar to the add but it divides and it dosent check for divide by zero
public Even div (Even arg) {

	Even result = new Even (value / arg.toInt());
	return(result);

}

//This method takes in a string of ints and then converts them all into spelled out words instead of ints
public String toString () {

String arg = "";

arg = (arg + value);//converts the Even into a string

String p = "";

	for(int i=0;i < arg.length();i++) {

		if(arg.charAt(i) == '-') p = (p + "negative ");

		if(arg.charAt(i) == '0') p = (p + "zero ");

		if(arg.charAt(i) == '1') p = (p + "one ");	

		if(arg.charAt(i) == '2') p = (p + "two ");

		if(arg.charAt(i) == '3') p = (p + "three ");

		if(arg.charAt(i) == '4') p = (p + "four ");

		if(arg.charAt(i) == '5') p = (p + "five ");

		if(arg.charAt(i) == '6') p = (p + "six ");

		if(arg.charAt(i) == '7') p = (p + "seven ");

		if(arg.charAt(i) == '8') p = (p + "eight ");

		if(arg.charAt(i) == '9') p = (p + "nine ");
		
	}

	return (p);
}		

}//closing bracket of the class
